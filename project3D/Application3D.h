#pragma once
#include "Application.h"
#include <glm/mat4x4.hpp>
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "tiny_obj_loader.h"
#include <imgui.h>
#include "Object.h"

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


	// Camera 
	Camera *				m_camera;
	glm::mat4				m_viewMatrix;
	glm::mat4				m_projectionMatrix;	

	struct Light {
		glm::vec3 direction;
		glm::vec3 diffuse;
		glm::vec3 specular;
	};

	int m_CurrentShader;

	Light					m_light;
	glm::vec3				m_ambientLight;

	// Objects
	Object	m_Bunny;
	Object	m_SoulSpear;
	Object	m_Buddha;
	Object	m_Dragon;
	Object	m_Lucy;

	/////////////////
	// Shader Sets //
	/////////////////
	// Phong with normal mapping
	aie::ShaderProgram		m_PhongNormal;
	// Phong without normal mapping
	aie::ShaderProgram		m_Phong;
	// Simple Shader
	aie::ShaderProgram		m_SimpleShader;
	
	///////////////////////
	// Mesh Declarations //
	///////////////////////
	// Quad 
	aie::Texture			m_gridTexture;
	Mesh					m_quadMesh;
	glm::mat4				m_quadTransform;
};