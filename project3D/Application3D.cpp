#include "Application3D.h"
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

Application3D::Application3D() {

}

Application3D::~Application3D() {
	 
}

bool Application3D::startup() {

	setBackgroundColour(0.25f, 0.25f, 0.25f);

	m_camera = new Camera();

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
		getWindowWidth() / (float)getWindowHeight(),
		0.1f, 1000.f);
	
	/////////////////////
	// Loading shaders //
	/////////////////////
	
	m_Phong.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");
	m_Phong.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");

	m_PhongNormal.loadShader(aie::eShaderStage::VERTEX,   "./shaders/phongNorm.vert");
	m_PhongNormal.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phongNorm.frag");

	m_SimpleShader.loadShader(aie::eShaderStage::VERTEX,   "./shaders/simple.vert");
	m_SimpleShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/simple.frag");
	
	if (m_Phong.link() == false) {
		printf("Phong Shader broke: %s\n", m_Phong.getLastError());
		return false;
	}

	if (m_PhongNormal.link() == false) {
		printf("Phone Normal Shader broke: %s\n", m_Phong.getLastError());
		return false;
	}

	if (m_SimpleShader.link() == false) {
		printf("Simple Shader broke: %s\n", m_Phong.getLastError());
		return false;
	}


	///////////////////
	// Loading Meshs //
	///////////////////

	m_Bunny.StartUp(glm::mat4{ 0.2f,0,0,0,
							   0,0.2f,0,0,
							   0,0,0.2f,0,
							   0,0,0,0.2f }, "./models/Bunny.obj");
	
	m_SoulSpear.StartUp(glm::mat4{ 0.2f,0,0,0,
								   0,0.2f,0,0,
								   0,0,0.2f,0,
								   -2,0,2,0.2f }, "./models/soulspear.obj");

	m_Buddha.StartUp(glm::mat4{  0.2f,0,0,0,
								 0,0.2f,0,0,
								 0,0,0.2f,0,
								0,0,-2,0.2f }, "./models/Buddha.obj");

	m_Dragon.StartUp(glm::mat4{ 0.2f,0,0,0,
								0,0.2f,0,0,
								0,0,0.2f,0,
								-2,0,-2,0.2f }, "./models/Dragon.obj");

	m_Lucy.StartUp(glm::mat4{ 0.2f,0,0,0,
							  0,0.2f,0,0,
							  0,0,0.2f,0,
							  2,0,2,0.2f }, "./models/Lucy.obj");

	// Initialising objects.
	m_quadMesh.InitialiseQuad();
	
	m_light.direction = { 1,1,1 };
	m_light.diffuse = { 1, 1, 0 };
	m_light.specular = { 1, 1, 0 };
	m_ambientLight = { 0.25f, 0.25f, 0.25f };

	return true;
}

void Application3D::shutdown() {

	delete m_camera;
	Gizmos::destroy();
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	// wipe the gizmos clean for this frame
	Gizmos::clear();
	
	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	m_camera->Update();

	// draw a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10),
			vec3(-10 + i, 0, -10),
			i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
			vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();

	if (input->isKeyDown(aie::INPUT_KEY_1)) {
		m_CurrentShader = 1;
	}

	if (input->isKeyDown(aie::INPUT_KEY_2)) {
		m_CurrentShader = 2;
	}
}
 
void Application3D::draw() {
	// wipe the screen to the background colour
	clearScreen();

	// update perspective in case window resized
	m_projectionMatrix = m_camera->GetProjectionMatrix(getWindowWidth(), getWindowHeight());
	m_viewMatrix = m_camera->GetViewMatrix();

	//////////////////
	// Imgui Things //
	//////////////////
	
	//m_light.direction = { 1,1,1 };
	ImGui::Begin("Light Direction");
	ImGui::SliderFloat("X", &m_light.direction.x, -10 ,10);
	ImGui::SliderFloat("Y", &m_light.direction.y, -10, 10);
	ImGui::SliderFloat("Z", &m_light.direction.z, -10, 10);
	ImGui::End();

	ImGui::Begin("Light Diffuse");
	ImGui::SliderFloat("X", &m_light.diffuse.x, 0, 1);
	ImGui::SliderFloat("Y", &m_light.diffuse.y, 0, 1);
	ImGui::SliderFloat("Z", &m_light.diffuse.z, 0, 1);
	ImGui::End();

	ImGui::Begin("Light Specular");
	ImGui::SliderFloat("X", &m_light.specular.x, 0, 1);
	ImGui::SliderFloat("Y", &m_light.specular.y, 0, 1);
	ImGui::SliderFloat("Z", &m_light.specular.z, 0, 1);
	ImGui::End();

	ImGui::Begin("Ambient Light");
	ImGui::SliderFloat("X", &m_ambientLight.x, 0, 1);
	ImGui::SliderFloat("Y", &m_ambientLight.y, 0, 1);
	ImGui::SliderFloat("Z", &m_ambientLight.z, 0, 1);
	ImGui::End();



	// Object Draws

	switch (m_CurrentShader) {
	case 1:
		m_Buddha.Draw(   this, m_Phong, m_CurrentShader, false);
		m_Bunny.Draw(    this, m_Phong, m_CurrentShader, false);
		m_Dragon.Draw(   this, m_Phong, m_CurrentShader, false);
		m_Lucy.Draw(     this, m_Phong, m_CurrentShader, false);
		m_SoulSpear.Draw(this, m_PhongNormal, m_CurrentShader, true);
		break;
	case 2:
		m_Buddha.Draw(this,    m_SimpleShader, m_CurrentShader, false);
		m_Bunny.Draw(this, m_SimpleShader, m_CurrentShader, false);
		m_Dragon.Draw(this, m_SimpleShader, m_CurrentShader, false);
		m_Lucy.Draw(this, m_SimpleShader, m_CurrentShader, false);
		m_SoulSpear.Draw(this, m_SimpleShader, m_CurrentShader, true);
	}

	// draw 3D gizmos
	Gizmos::draw(m_projectionMatrix * m_viewMatrix);

	// draw 2D gizmos using an orthogonal projection matrix (or screen dimensions)
	Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());
}