#pragma once
#include <glm/glm.hpp>
#include "Gizmos.h"
#include "OBJMesh.h"
#include "Shader.h"

class Application3D;

class Object
{
public:
	void StartUp(glm::mat4 transform, char* loadLocation);
	void Draw(const Application3D * app, aie::ShaderProgram & shader, int shadernum, bool isSoulSpear);

	Object();
	~Object();

	aie::OBJMesh m_Mesh;
	glm::mat4 m_Transform;
};

