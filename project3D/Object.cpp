#include "Object.h"
#include "Application3D.h"
#include <glm\glm.hpp>
#include <glm\ext.hpp>

void Object::StartUp(glm::mat4 transform, char* loadLocation)
{
	if (m_Mesh.load(loadLocation, true, true) == false) {
		printf("Error Loading Mesh");
	}
	m_Transform = transform;
}

void Object::Draw(const  Application3D * app, aie::ShaderProgram & shader, int shadernum, bool isSoulSpear)
{	
	shader.bind();

	auto pvm = app->m_projectionMatrix * app->m_viewMatrix * m_Transform;

	switch (shadernum) {
	case 1:
		shader.bindUniform("Ia", app->m_ambientLight);
		shader.bindUniform("Id", app->m_light.diffuse);
		shader.bindUniform("Is", app->m_light.specular);
		shader.bindUniform("LightDirection", app->m_light.direction);
		shader.bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_Transform)));
		shader.bindUniform("CameraPosition", glm::vec3(glm::inverse(app->m_viewMatrix)[3]));
		if (isSoulSpear)
			shader.bindUniform("DiffuseTexture", 0);

		shader.bindUniform("ProjectionViewModel", pvm);
		break;

	case 2:
		shader.bindUniform("ProjectionViewModel", pvm);
		break;
	}

	m_Mesh.draw();
}

Object::Object()
{
}

Object::~Object()
{
}