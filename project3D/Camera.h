#pragma once
#include <glm\glm.hpp>

class Camera
{
public:
	~Camera();
public:
	Camera() : theta(0), phi(-20), position(-10, 4, 0) {}
	
	const float deg2Rad = 3.14158f / 180.0f;
	
	void Update();

	glm::mat4 GetProjectionMatrix(float w, float h);
	glm::mat4 GetViewMatrix();
	glm::vec3 GetPosition() { return position; }

private:
	
	int lastMouseX;
	int lastMouseY;
	
	float theta;
	float phi;

	glm::vec3 position;
};