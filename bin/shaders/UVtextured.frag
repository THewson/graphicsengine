// a simple textured shader
#version 410

in vec4 vPosition;
in vec2 vTexCoord;

uniform sampler2D diffuseTexture;

out vec4 FragColour;

void main() {	
	vec2 UV = vPosition.xy;

	FragColour = texture(diffuseTexture, UV);
}